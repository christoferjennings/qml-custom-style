Attempting to make a custom style, "AStyle", that can be used in a static-compiled Qt 6.1.1 app, using CMake.

Started by reading the "Creating Custom Style" docs...
https://doc.qt.io/qt-6/qtquickcontrols2-customize.html#creating-a-custom-style


And "Compile-Time Style Selection"...

https://doc.qt.io/qt-6/qtquickcontrols2-customize.html#creating-a-custom-style


And "QML Import Path"...

https://doc.qt.io/qt-6/qtqml-syntax-imports.html#qml-import-path


Trying to figure out the QML import path led me to these places...

https://doc.qt.io/qtcreator/creator-qml-modules-with-plugins.html#importing-qml-modules

and...

https://stackoverflow.com/questions/64781283/how-to-use-include-qml-module-in-cmake-desktop-application
