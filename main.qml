import QtQuick
import QtQuick.Window
import QtQuick.Controls

// Comment out next line to see the app without AStyle
import AStyle

Window {
    width: 500
    height: 150
    visible: true
    title: qsTr("Try to use a custom style")

    Column {
        anchors.centerIn: parent
        Text {
            text: qsTr("If the custom style works, this button should be red.")
        }
        Button {
            text: qsTr("I'm a button!, but am I red?")
        }
    }
}
