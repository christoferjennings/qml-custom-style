//
//  This is copied from Qt's Basic style. All I'm doing is making the color red.
//

import QtQuick
import QtQuick.Controls.impl
import QtQuick.Templates as T

// Qt Creator warns that I'm recursively instantiating Button.
T.Button {
    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        visible: !control.flat || control.down || control.checked || control.highlighted

        color: "red"
        //color: Color.blend(control.checked || control.highlighted ? control.palette.dark : control.palette.button,
        //                                                            control.palette.mid, control.down ? 0.5 : 0.0)

        border.color: control.palette.highlight
        border.width: control.visualFocus ? 2 : 0
    }
}
